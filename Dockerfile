FROM alpine

RUN apk add --no-cache curl \
 && export FRP_VERSION=$(curl -sSI https://github.com/fatedier/frp/releases/latest | grep -i location | awk -F/ '{print $8}' | sed 's/^.//' | sed 's/.$//') \
 && curl -fSL https://github.com/fatedier/frp/releases/download/v${FRP_VERSION}/frp_${FRP_VERSION}_linux_amd64.tar.gz -o frp.tar.gz \
 && tar -zxv -f frp.tar.gz \
 && mv frp_${FRP_VERSION}_linux_amd64 /frp \
 && chmod u+x /frp/frp* \
 && mkdir -p /conf \
 && rm -rf /var/cache/apk/* ~/.cache frp.tar.gz


WORKDIR /frp

COPY frps.ini /conf/

VOLUME /conf

ENTRYPOINT ["/frp/frps", "-c", "/conf/frps.ini"]
